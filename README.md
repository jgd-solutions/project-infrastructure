# project-infrastructure

Common data required to host a project.

Opposed to each project having an
arbitrary set of project data, this repo allows:

- Tracking and revisions of project data
- Consistency amoung projects
- Starting point for new projects

This repo is to include generic, shared forms of the following:

- configuration files
- boiler-plate markup
- licensing & common headers

This repo is not to include:

- code
- tools

## Design Considerations

Since this repository contains multiple separate entities, it's important to ensure commits reflect a single unit of change to a single entity of the project.

## Usage

### Getting the Project

Add this repository as a sub-module of your repository. Git submodules are usually horrendeous, but make sense in this situation, with a collection of config files.

In your repository,

If the project is within the _jgd-solutions_ group, use a relative URL:

```bash
git submodule add ../project-infrastructure.git
```

Otherwise,

```bash
git submodule add https://gitlab.com/jgd-solutions/project-infrastructure.git
```

All infrastructure files are then housed within this project's sub-directories.
Other files should be ignored by consuming projects, as they are part of
maintaining this project - for example, README.md.

### Non-Git Configuration Files

Add relative symlinks in your project to the desired submodule files.

Ex 1.

```bash
ln -s project-infrastructure/project-infrastructure/clang-tools/clang-format .clang-format
```

Ex 2.

```bash
ln -s project-infrastructure/project-infrastructure/licenses/MIT.md LICENSE.md
```

### Git Configuration Files

As of [git
2.32.0](https://github.com/git/git/blob/master/Documentation/RelNotes/2.32.0.txt),
git will no longer follow symbolic links on .gitattributes, .gitignore, and
.mailmap files. Instead of using relative symlinks, edit your project's local
git config to point to the files in the project-infrastructure submodule

Ex.

```bash
git config core.excludesfile project-infrastructure/project-infrastructure/git/gitignore
```

The local config will be stored in `<your-project>/.git/config`

### Git Pre-Commit Hook

Git hooks are often avoided in favour of CI, because they aren't designed to be
tracked by git. However, Python environments generate their dependency files
(_Pipfile_ & _Pipfile.lock_ with `pipenv`, _requirements.txt_ with `pip`) from a
virtual environment, which are also untracked. As such, before even reaching
CI, it must be ensured that these files were generated from the local venv.

From the repository root link _pre-commit_ into git's hooks so git will run it
before each commit. Source path is relative to destination link:

```bash
ln -s ../../project-infrastructure/git/pre-commit .git/hooks/pre-commit
```

### Markdown Files

Without an external tool, all that can be done is to provide a relative link to
the enclosed markdown files:

Ex.

```markdown
[Setup python](project-infrastructure/project-infrastructure/readme/setup_python.md)
```


List of open-source licenses: https://spdx.org/licenses/