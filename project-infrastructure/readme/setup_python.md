## Setup Python

1. Install Python 3

    Download from the [official site](https://www.python.org/downloads/), or install through your system's package manager, which is recommended.

    ```bash
    sudo pacman -S python    # arch-linux
    sudo apt install python3 # debian/Ubuntu
    brew install python      # macos
    ```

2. Install [Pipenv](https://pipenv-fork.readthedocs.io/en/latest/), the
    recommended Python environment management tool. It deals with *pip* and
    *virtualenv* automatically, and provides reproducibility. Moving forward, exclusively use `pipenv`.

    ```bash
    pip install pipenv
    ```

3. Activate a python virtual environment from the project's root.

    ```bash
    pipenv shell
    ```

    The virtual environment can be exitted using the `exit` command.

4. Install Python Dependencies

    *Pipfile* and *Pipfile.lock* files should exist within the repository's
    root. Synchronize dependencies in the virtual environment with those
    described in the *pipenv* files using:

    ```bash
    pipenv sync
    ```

    If Python dependencies need to be modified, remember to update the requirements
    file using:

    ```bash
    pipenv lock
    ```
