from conans import ConanFile, CMake, tools


class BaseRecipe(ConanFile):
    license = "Proprietary"
    author = "JGD Solutions"
    settings = "os", "compiler", "build_type", "arch"
    options = {"with_tests": [False, True]}
    default_options = {"with_tests": False}
    generators = "cmake_find_package_multi"
    no_copy_source = True

    def _should_run_tests(self):
        return self.options.with_tests and not tools.cross_building(self)

    def export(self):
        self.copy("project-infrastructure/project-infrastructure/conan/base_recipe.py")
        self.copy("LICENSE.md")

    def export_sources(self):
        self.copy("CMakeLists.txt")
        self.copy(f"{self.name}/*")
        self.copy("tests/*.hpp")
        self.copy("tests/*")
        self.copy("cmake/*")

    def _configure_cmake(self, option_prefix=None):
        cmake = CMake(self)
        cmake.verbose = True
        if option_prefix is None:
            option_prefix = self.name.upper().replace("-", "_").replace(" ", "_")
        cmake.definitions[f"{option_prefix}_BUILD_TESTING"] = self._should_run_tests()
        cmake.configure()
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()
        if self._should_run_tests():
            cmake.test()

    def package(self):
        self.copy(f"{self.name}/*.hpp", dst="include")
        self.copy(f"lib/{self.name}.lib", dst="lib", keep_path=False)
        self.copy(f"lib/{self.name}.dll", dst="bin", keep_path=False)
        self.copy(f"lib/{self.name}.so", dst="lib", keep_path=False)
        self.copy(f"lib/{self.name}.dylib", dst="lib", keep_path=False)
        self.copy(f"lib/{self.name}.a", dst="lib", keep_path=False)

    def package_info(self):
        lib_name = self.name
        if self.name.startswith("lib") and len(self.name) > len("lib"):
            lib_name = lib_name[len("lib"):]

        self.cpp_info.libs = [lib_name]
